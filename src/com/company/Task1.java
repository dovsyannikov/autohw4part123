package com.company;

public class Task1 {
    /*Необходимо вывести cпоследовательность: 3 9 15 21 27 33 39 45 51 57 63 69 (сделать через do/while и for)*/

    public static void numbersFOR(){
        for (int i = 0; i < 12; i++) {
            System.out.print(3 + (6*i) + " ");
        }
        System.out.println();
    }
    public static void numbersDoWhile(){
        System.out.print(3 + " ");
        int i = 3;
        do {
            i += 6;
            System.out.print(i + " ");
        }while (i < 69);
    }
}
