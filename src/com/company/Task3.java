package com.company;

import java.util.Scanner;

public class Task3 {
    /*Вы заходите в атб и у вас 200 грн.
    - вы набираете товар на 202 грн, и тогда выводе “не хватает 2 grn”,
    - вы набираете продуктов на 200 грн и выводе счастливчик,
    - набираете продуктов на 100 грн  и выводе – пойду возьму еще что то
    -вводите другое число и выводите что то пошло не так.
    Использовать оператор Switch,  должно выводиться на экран только 1 сообщение, если нет совпадений, то сообщение по дефолту.
    */

    private static int getInt(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }
    public static void atb (){
        int sum = getInt("Введите сумму на которую вы скупились");
        switch (sum){
            case 202:
                System.out.println("не хватает 2грн");
                break;
            case 200:
                System.out.println("Vi Shastlivshik");
                break;
            case 100:
                System.out.println("пойду возьму еще что то");
                break;
            default:
                System.out.println("Smth went wrong");
                break;
        }
    }
}
